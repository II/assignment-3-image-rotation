#pragma once

#include "image.h"
#include <stdint.h>
#include <stdio.h>

#define BMP_SIGNATURE 0x4D42
#define BITS_PER_PIXEL 24
#define BMP_PADDING 4
#define BYTES_PER_PIXEL 3
#define BMP_HEADER_SIZE 40
#define DEFAULT_PELS_PER_METER 2834
#define NO_COMPRESSION 0
#define NO_CLR_USED 0

// Структура для BMP файлового заголовка
struct __attribute__((__packed__)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

// Статусы чтения из файла
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

// Статусы записи в файл
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    // Дополнительные коды ошибок
};

// Функция для чтения изображения из BMP файла
enum read_status from_bmp(FILE* file, struct image* img);

// Функция для записи изображения в BMP файл
enum write_status to_bmp(FILE* file, const struct image* img);
