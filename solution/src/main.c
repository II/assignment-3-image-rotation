#include "bmp_format.h"
#include "file_utils.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

#define REQUIRED_ARG_COUNT 4

static inline int normalize_angle(int angle) {
    return (angle % 360 + 360) % 360;
}

int main(int argc, char* argv[]) {
    if (argc != REQUIRED_ARG_COUNT) {
        fprintf(stderr, "Использование: %s <входное изображение> <выходное изображение> <угол>\n", argv[0]);
        return 1;
    }

    const char* input_filename = argv[1];
    const char* output_filename = argv[2];

    int angle = atoi(argv[3]) * (-1);
    angle = normalize_angle(angle);

    // Проверка на допустимые значения угла
    if (angle != 0 && angle != 90 && angle != 180 && angle != 270) {
        fprintf(stderr, "Угол должен быть одним из следующих значений: 0, 90, -90, 180, -180, 270, -270\n");
        return 1;
    }

    FILE* input_file = open_file_read(input_filename);
    if (input_file == NULL) {
        perror("Ошибка при открытии входного файла");
        return 1;
    }

    struct image img;
    enum read_status read_status = from_bmp(input_file, &img);
    fclose(input_file);
    if (read_status != READ_OK) {
        fprintf(stderr, "Ошибка чтения BMP файла: %d\n", read_status);
        return 1;
    }

    struct image rotated_img = rotate(&img, angle);
    destroy_image(&img);
    if (rotated_img.pixels == NULL) {
        fprintf(stderr, "Ошибка при повороте изображения\n");
        return 1;
    }

    FILE* output_file = open_file_write(output_filename);
    if (output_file == NULL) {
        perror("Ошибка при открытии выходного файла");
        destroy_image(&rotated_img);
        return 1;
    }

    enum write_status write_status = to_bmp(output_file, &rotated_img);
    fclose(output_file);
    destroy_image(&rotated_img);

    if (write_status != WRITE_OK) {
        fprintf(stderr, "Ошибка записи BMP файла: %d\n", write_status);
        return 1;
    }

    printf("Изображение успешно обработано и сохранено в %s\n", output_filename);
    return 0;
}
