#include "rotate.h"
#include "image.h"

struct image rotate(const struct image* img, int angle) {
    struct image rotated_img;

    // Проверка угла и инициализация изображения
    switch (angle % 360) {
        case 90:
        case 270:
            rotated_img = create_image(img->height, img->width);
            break;
        case 180:
        case 0:
            rotated_img = create_image(img->width, img->height);
            break;
        default:
            // Для неподдерживаемых углов возвращаем пустое изображение
            return (struct image){0, 0, NULL};
    }

    if (!rotated_img.pixels) {
        return (struct image){0, 0, NULL};  // В случае неудачи при выделении памяти
    }

    // Осуществляем поворот
    for (uint64_t y = 0; y < img->height; ++y) {
        for (uint64_t x = 0; x < img->width; ++x) {
            struct pixel current_pixel = img->pixels[y * img->width + x];
            switch (angle % 360) {
                case 90:
                    rotated_img.pixels[x * rotated_img.width + (rotated_img.width - y - 1)] = current_pixel;
                    break;
                case 180:
                    rotated_img.pixels[(rotated_img.height - y - 1) * rotated_img.width + (rotated_img.width - x - 1)] = current_pixel;
                    break;
                case 270:
                    rotated_img.pixels[(rotated_img.height - x - 1) * rotated_img.width + y] = current_pixel;
                    break;
                case 0:
                    rotated_img.pixels[y * rotated_img.width + x] = current_pixel;
                    break;
            }
        }
    }

    return rotated_img;
}
